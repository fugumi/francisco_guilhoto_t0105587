import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import indexMovies from '@/components/index'
import createMovies from '@/components/create'
import thisYearMovies from '@/components/thisyear'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
    path: '/movies/index',
    name: 'movies.index',
    component: indexMovies
  },
  {
        path: '/movies/create',
        name: 'movies.create',
        component: createMovies
    },
    {
          path: '/movies/thisyear',
          name: 'movies.thisyear',
          component: thisYearMovies
      },


  ]
})
